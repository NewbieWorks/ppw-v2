from django.urls import path
from .views import index, guess, casun, forming
from django.conf import settings
from django.conf.urls.static import static

app_name = "profiles"
urlpatterns = [
    path('', index, name='index'),
    path('index', index, name='index'),
    path('guess-book', guess, name='guess'),
    path('big8', casun, name='casun'),
    path('forming', forming, name='forming'),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)