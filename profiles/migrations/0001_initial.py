# Generated by Django 2.1.1 on 2018-10-02 10:02

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='JadwalPribadi',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tanggal', models.DateField()),
                ('nama_kegiatan', models.TextField()),
                ('tempat', models.TextField()),
                ('kategori', models.TextField()),
            ],
        ),
    ]
